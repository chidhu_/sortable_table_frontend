import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import 'bootstrap/dist/css/bootstrap.min.css';
// var Emitter = require('tiny-emitter');
// var emitter = new Emitter();
var emitter = require('tiny-emitter');
createApp(App).use(store).use(router).use(emitter).mount('#app')
