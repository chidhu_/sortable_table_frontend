import { createStore } from 'vuex'

export default createStore({
  state: {
    selectedParentNodes:null,
    allSelectedNodes:[]
  },
  mutations: {
    SET_SELECTED_PARENT_NODES(state,payload){
      console.log(payload)
      // state.selectedParentNodes.push(payload);
      state.selectedParentNodes=payload;
    },
    SET_ALL_SELECTED_NODES(state,payload){
      state.allSelectedNodes = payload;
    }
  },
  actions: {
  },
  modules: {
  }
})
